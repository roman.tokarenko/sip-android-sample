package com.example.connectionservicedemo;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.example.connectionservicedemo.ConnectionService.ConnectionServiceHelper;

import static android.telecom.Connection.STATE_ACTIVE;
import static android.telephony.TelephonyManager.EXTRA_STATE_RINGING;

public class TestConnectionServiceApp extends Application {
    private static TestConnectionServiceApp mInstance = null;
    private ConnectionServiceHelper mConnectionServiceHelper = null;

    private final BroadcastReceiver mPhoneStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            Log.d("TestConnectionService", "SystemCallStateReceiver: action is " + intent.getAction());

            String state2 = "";

            if (TelephonyManager.ACTION_PHONE_STATE_CHANGED.equals(action)) {
                final String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
                state2 = state;
                final String incomingNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
                Log.d("TestConnectionService", "SystemCallStateReceiver: state is " + state + " number is " + incomingNumber);
            }

            int currentConnectionStatus = mConnectionServiceHelper.getStatus();
            Log.d("TestConnectionService", "SystemCallStateReceiver: currentConnectionStatus is " + currentConnectionStatus);

            //todo How can I catch click on Accept button in the system incoming call dialog
            // for GSM call that will be accepted in another calling app?
            if (currentConnectionStatus == STATE_ACTIVE && state2.equals(EXTRA_STATE_RINGING)) {
                Log.d("TestConnectionService", "SystemCallStateReceiver: trying to put the current call on hold");
                mConnectionServiceHelper.setCurrentCallOnHold();
            }
        }
    };

    //TelephonyManager.ACTION_PRECISE_CALL_STATE_CHANGED. It is hidden and requires the READ_PRECISE_PHONE_STATE permission which is a system permission.
    // Look at it TelephonyManager.

    /*
    //from TelephonyRegistry
    private void broadcastPreciseCallStateChanged(int ringingCallState, int foregroundCallState,
            int backgroundCallState, int disconnectCause, int preciseDisconnectCause) {
        Intent intent = new Intent(TelephonyManager.ACTION_PRECISE_CALL_STATE_CHANGED);
        intent.putExtra(TelephonyManager.EXTRA_RINGING_CALL_STATE, ringingCallState);
        intent.putExtra(TelephonyManager.EXTRA_FOREGROUND_CALL_STATE, foregroundCallState);
        intent.putExtra(TelephonyManager.EXTRA_BACKGROUND_CALL_STATE, backgroundCallState);
        intent.putExtra(TelephonyManager.EXTRA_DISCONNECT_CAUSE, disconnectCause);
        intent.putExtra(TelephonyManager.EXTRA_PRECISE_DISCONNECT_CAUSE, preciseDisconnectCause);
        mContext.sendBroadcastAsUser(intent, UserHandle.ALL,
                android.Manifest.permission.READ_PRECISE_PHONE_STATE);
    }*/

    //About using PreciseCallState
    //https://stackoverflow.com/questions/13134331/cannot-detect-when-outgoing-call-is-answered-in-android/29490832#29490832

    // About different call states
    // https://stackoverflow.com/questions/6290347/what-does-the-different-call-states-in-the-android-telephony-stack-represent

    // CallManager.registerForPreciseCallStateChanged. Can we use it?
    // CallManager.registerPhone

    private static final PhoneStateListener mPhoneStateListener = new PhoneStateListener() {
        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            super.onCallStateChanged(state, incomingNumber);
            Log.d("TestConnectionService", "PhoneStateListener: onCallStateChanged to " + state + " for " + incomingNumber);
        }

        /*@Override
        public void onServiceStateChanged(ServiceState serviceState) {
            super.onServiceStateChanged(serviceState);
            Log.d("TestConnectionService", "PhoneStateListener: onServiceStateChanged to " + serviceState);
        }

        @Override
        public void onCellInfoChanged(List<CellInfo> cellInfo) {
            super.onCellInfoChanged(cellInfo);
            Log.d("TestConnectionService", "PhoneStateListener: onCellInfoChanged to " + cellInfo);
        }

        @Override
        public void onDataConnectionStateChanged(int state) {
            super.onDataConnectionStateChanged(state);
            Log.d("TestConnectionService", "PhoneStateListener: onDataConnectionStateChanged to " + state);
        }*/
    };

    @Override
    public void onCreate() {
        super.onCreate();
        synchronized (TestConnectionServiceApp.class) {
            mInstance = this;
        }

        mConnectionServiceHelper = new ConnectionServiceHelper(getApplicationContext());

        final IntentFilter filter = new IntentFilter();
        filter.addAction(TelephonyManager.ACTION_PHONE_STATE_CHANGED);
        registerReceiver(mPhoneStateReceiver, filter);

        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        telephonyManager.listen(
                mPhoneStateListener,
                PhoneStateListener.LISTEN_CALL_STATE |
                        PhoneStateListener.LISTEN_CELL_INFO |
                        PhoneStateListener.LISTEN_DATA_CONNECTION_STATE
        );

        /*Method[] methods1 = PhoneStateListener.class.getMethods();
        Method[] methods2 = PhoneStateListener.class.getDeclaredMethods();
        for (Method m1: methods1) {
            Log.d("TestConnectionService", "PhoneStateListener method: " + m1);
        }
        for (Method m2: methods2) {
            Log.d("TestConnectionService", "PhoneStateListener declared method: " + m2);
        }*/
    }

    public static TestConnectionServiceApp getInstance() {
        synchronized (TestConnectionServiceApp.class) {
            return mInstance;
        }
    }

    public ConnectionServiceHelper getConnectionServiceHelper() {
        return mConnectionServiceHelper;
    }


}
