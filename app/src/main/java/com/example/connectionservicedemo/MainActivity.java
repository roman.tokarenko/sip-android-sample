package com.example.connectionservicedemo;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.telecom.Connection;
import android.telecom.DisconnectCause;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.PermissionChecker;

import com.example.connectionservicedemo.ConnectionService.ConnectionServiceHelper;

public class MainActivity extends AppCompatActivity implements ConnectionServiceHelper.Listener {
    private TextView mCallStatusView;

    private View mBtnSimulateIncomingCall;
    private View mBtnAcceptCall;
    private View mBtnEndCall;
    private View mBtnUnholdCall;

    private ConnectionServiceHelper mConnectionServiceHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mCallStatusView = findViewById(R.id.connection_status);
        mBtnSimulateIncomingCall = findViewById(R.id.btn_incoming_call);
        mBtnAcceptCall = findViewById(R.id.btn_accept);
        mBtnEndCall = findViewById(R.id.btn_end);
        mBtnUnholdCall = findViewById(R.id.btn_unhold);
        mConnectionServiceHelper = TestConnectionServiceApp.getInstance().getConnectionServiceHelper();

        mCallStatusView.setText("IDLE");

        mBtnSimulateIncomingCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallStatusView.setText("Simultaing incoming call");
                mBtnSimulateIncomingCall.setEnabled(false);
                mBtnAcceptCall.setEnabled(false);
                mBtnEndCall.setEnabled(false);
                TestConnectionServiceApp.getInstance().getConnectionServiceHelper().placeNewIncomingCall("TEST_CALLER");
            }
        });
        mBtnSimulateIncomingCall.setEnabled(true);

        mBtnAcceptCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TestConnectionServiceApp.getInstance().getConnectionServiceHelper().acceptCall2();
            }
        });
        mBtnAcceptCall.setEnabled(false);

        mBtnEndCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TestConnectionServiceApp.getInstance().getConnectionServiceHelper().endCall(DisconnectCause.LOCAL);
            }
        });
        mBtnEndCall.setEnabled(false);

        mBtnUnholdCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallStatusView.setText("ACCEPTED");
                mBtnSimulateIncomingCall.setEnabled(false);
                mBtnAcceptCall.setEnabled(false);
                mBtnUnholdCall.setEnabled(false);
                mBtnEndCall.setEnabled(true);
                TestConnectionServiceApp.getInstance().getConnectionServiceHelper().setCurrentCallUnhold();
            }
        });
        mBtnUnholdCall.setEnabled(false);

        String[] permissions = {Manifest.permission.READ_PHONE_STATE};
        requestPermissions(permissions);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mConnectionServiceHelper.setListener(this);

        switch (mConnectionServiceHelper.getStatus()) {
            case Connection.STATE_RINGING:
                onRinging();
                break;
            case Connection.STATE_ACTIVE:
                onCallAccepted();
                break;
            case Connection.STATE_HOLDING:
                onCallHeld();
                break;
            case Connection.STATE_DISCONNECTED:
            default:
                onCallEnded();
                break;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mConnectionServiceHelper.setListener(null);
    }

    @Override
    public void onRinging() {
        Log.d("TestConnectionService", "Activity: onRinging");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mCallStatusView.setText("RINGING");
                mBtnSimulateIncomingCall.setEnabled(false);
                mBtnAcceptCall.setEnabled(true);
                mBtnEndCall.setEnabled(true);
            }
        });

    }

    @Override
    public void onCallAccepted() {
        Log.d("TestConnectionService", "Activity: onCallAccepted");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mCallStatusView.setText("ACCEPTED");
                mBtnSimulateIncomingCall.setEnabled(false);
                mBtnAcceptCall.setEnabled(false);
                mBtnEndCall.setEnabled(true);
            }
        });
    }

    @Override
    public void onCallEnded() {
        Log.d("TestConnectionService", "Activity: onCallEnded");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mCallStatusView.setText("IDLE");
                mBtnSimulateIncomingCall.setEnabled(true);
                mBtnAcceptCall.setEnabled(false);
                mBtnEndCall.setEnabled(false);
                mBtnUnholdCall.setEnabled(false);
            }
        });
    }

    @Override
    public void onCallHeld() {
        Log.d("TestConnectionService", "Activity: onCallHeld");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mCallStatusView.setText("HOLD");
                mBtnSimulateIncomingCall.setEnabled(false);
                mBtnAcceptCall.setEnabled(false);
                mBtnEndCall.setEnabled(false);
                mBtnUnholdCall.setEnabled(true);
            }
        });
    }


    private int checkSelfPermission(String[] permissions) {
        for (String permission : permissions) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                return PermissionChecker.PERMISSION_DENIED;
            }
        }
        return PermissionChecker.PERMISSION_GRANTED;
    }

    private void requestPermissions(String[] permissions) {
        if (checkSelfPermission(permissions) != PermissionChecker.PERMISSION_GRANTED) {
            requestPermissions(permissions, 0);
        }
    }
}
