package com.example.connectionservicedemo.ConnectionService;

import android.content.ComponentName;
import android.content.Context;

import android.telecom.Connection;
import android.telecom.DisconnectCause;
import android.telecom.PhoneAccount;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomManager;
import android.util.Log;
import android.widget.Toast;

import static android.content.Context.TELECOM_SERVICE;

public class ConnectionServiceHelper {
    public interface Listener {
        void onRinging();

        void onCallAccepted();

        void onCallEnded();

        void onCallHeld();
    }

    /*
    TelecomManager tm
    tm.placeCall(); to trigger ConnectionService.onCreateOutgoingConnection.
    */


    /*
    Crucial questions:
    1. How the sequence of actions should work?
    2. When a new call is incoming will it be two instances of Connection at the same time?
    3. Check if an active call can be put on hold.
    */

    private final Context mCtx;
    private TestConnection mCurrentConnection = null;
    private TestConnection mAnotherCurrentConnection = null;
    private Listener mListener = null;

    public ConnectionServiceHelper(Context ctx) {
        mCtx = ctx;
    }

    public TestConnection getNewConnection() {
        final TestConnection c = new TestConnection();
        mCurrentConnection = c;
        return c;
    }

    public TestConnection getNewConnection2() {
        final TestConnection c = new TestConnection();
        if (mCurrentConnection != null) {
            Log.e("TestConnectionService", "getNewConnection2: mCurrentConnection " + mCurrentConnection);
            Log.e("TestConnectionService", "getNewConnection2: mCurrentConnection state " + mCurrentConnection.getState());
            if (mCurrentConnection.getState() == Connection.STATE_ACTIVE) {
                mAnotherCurrentConnection = c;
                mCurrentConnection.setOnHold();
                Log.e("TestConnectionService", "getNewConnection2: setting connection " + mCurrentConnection + "on hold");
            }
        } else {
            mCurrentConnection = c;
            Log.e("TestConnectionService", "getNewConnection2: assigning mCurrentConnection");
        }
        return c;
    }

    public void setListener(final Listener l) {
        mListener = l;
    }

    private PhoneAccountHandle getAccountHandle(final TelecomManager tm) {
        PhoneAccountHandle handle = new PhoneAccountHandle(new ComponentName(mCtx, TestConnectionService.class), "testConnectionUserID");
        PhoneAccount account = new PhoneAccount.Builder(handle, "testConnectionUserID")
                .setCapabilities(PhoneAccount.CAPABILITY_SELF_MANAGED)
                //.setIcon(Icon.createWithResource(this, R.drawable.ic_launcher_dr))
                //.setHighlightColor(0xff2ca5e0)
                .addSupportedUriScheme("sip")
                .build();
        tm.registerPhoneAccount(account);
        return handle;
    }

    public void placeNewIncomingCall(final String caller) {
        if (mCurrentConnection != null) {
            Log.e("TestConnectionService", "placeNewIncomingCall: connection already exists");
            Toast.makeText(mCtx, "TestConnectionService::placeNewIncomingCall: connection already exists", Toast.LENGTH_LONG).show();
            return;
        }

        final TelecomManager tm = (TelecomManager) mCtx.getSystemService(TELECOM_SERVICE);
        tm.addNewIncomingCall(getAccountHandle(tm), null);
    }

    public void acceptCall() {
        Log.e("TestConnectionService", "acceptCall: current connection is " + mCurrentConnection);
        if (mCurrentConnection != null) {

            mCurrentConnection.setActive();

            if (mListener != null) {
                mListener.onCallAccepted();
            }
        }
    }

    //todo Try and use it for set current active call on hold
    public void acceptCall2() {
        Log.e("TestConnectionService", "acceptCall2: current connection is " + mCurrentConnection);
        if (mCurrentConnection != null) {

            if (mCurrentConnection.getState() == Connection.STATE_HOLDING) {
                mAnotherCurrentConnection.setActive();
                Log.e("TestConnectionService", "acceptCall2: current active connection is " + mAnotherCurrentConnection);
                Log.e("TestConnectionService", "acceptCall2: held connection is " + mCurrentConnection);
            } else {
                mCurrentConnection.setActive();
                Log.e("TestConnectionService", "acceptCall2: current active connection is " + mCurrentConnection);
            }

            if (mListener != null) {
                mListener.onCallAccepted();
            }
        }
    }

    public void setCurrentCallOnHold() {
        if (mCurrentConnection != null) {
            Log.e("TestConnectionService", "setCurrentCallOnHold: current connection is " + mCurrentConnection);

            mCurrentConnection.setOnHold();
            if (mListener != null) {
                mListener.onCallHeld();
            }
        }
    }

    public void setCurrentCallUnhold() {
        if (mCurrentConnection != null) {
            Log.e("TestConnectionService", "setCurrentCallUnhold: current connection is " + mCurrentConnection);

            mCurrentConnection.setActive();
        }
    }

    public void endCall(final int cause) {
        Log.e("TestConnectionService", "endCall: current connection is " + mCurrentConnection + " cause is " + cause);
        if (mCurrentConnection != null) {
            mCurrentConnection.setDisconnected(new DisconnectCause(cause));
            mCurrentConnection.destroy();
            mCurrentConnection = null;

            if (mListener != null) {
                mListener.onCallEnded();
            }
        }
    }

    public void notifyRinging() {
        Log.e("TestConnectionService", "notifyRinging: current connection is " + mCurrentConnection);
        if (mListener != null) {
            mListener.onRinging();
        }
    }

    public int getStatus() {
        if (mCurrentConnection == null) {
            return Connection.STATE_DISCONNECTED;
        }

        return mCurrentConnection.getState();
    }
}
