package com.example.connectionservicedemo.ConnectionService;

import android.telecom.Connection;
import android.telecom.ConnectionRequest;
import android.telecom.ConnectionService;
import android.telecom.PhoneAccountHandle;
import android.util.Log;
import android.widget.Toast;

import com.example.connectionservicedemo.TestConnectionServiceApp;

import static android.telecom.TelecomManager.PRESENTATION_ALLOWED;

public class TestConnectionService extends ConnectionService {

    @Override
    public Connection onCreateOutgoingConnection(PhoneAccountHandle connectionManagerPhoneAccount,
                                                 ConnectionRequest request) {
        Log.d("TestConnectionService", "onCreateOutgoingConnection");
        final ConnectionServiceHelper serviceHelper = TestConnectionServiceApp.getInstance().getConnectionServiceHelper();

        final TestConnection connection = serviceHelper.getNewConnection();
        connection.setDialing();
        return connection;
    }

    @Override
    public void onCreateOutgoingConnectionFailed(PhoneAccountHandle connectionManagerPhoneAccount,
                                                 ConnectionRequest request) {
        Log.d("TestConnectionService", "onCreateOutgoingConnectionFailed");
        Toast.makeText(getApplicationContext(), "TestConnectionService::onCreateOutgoingConnectionFailed()", Toast.LENGTH_LONG).show();
    }

    @Override
    public Connection onCreateIncomingConnection(PhoneAccountHandle connectionManagerPhoneAccount,
                                                 ConnectionRequest request) {
        Log.d("TestConnectionService", "onCreateIncomingConnection");
        final ConnectionServiceHelper serviceHelper = TestConnectionServiceApp.getInstance().getConnectionServiceHelper();

        final TestConnection connection = serviceHelper.getNewConnection2();
        connection.setCallerDisplayName("Caller Name", PRESENTATION_ALLOWED);
        connection.setRinging();
        return connection;
    }

    @Override
    public void onCreateIncomingConnectionFailed(PhoneAccountHandle connectionManagerPhoneAccount,
                                                 ConnectionRequest request) {
        Log.d("TestConnectionService", "onCreateIncomingConnectionFailed");
        Toast.makeText(getApplicationContext(), "TestConnectionService::onCreateIncomingConnectionFailed()", Toast.LENGTH_LONG).show();
    }
}
