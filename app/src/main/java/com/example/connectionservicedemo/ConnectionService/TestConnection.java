package com.example.connectionservicedemo.ConnectionService;

import android.os.Build;
import android.os.Bundle;
import android.telecom.CallAudioState;
import android.telecom.Connection;
import android.telecom.DisconnectCause;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.example.connectionservicedemo.TestConnectionServiceApp;

@RequiresApi(api = Build.VERSION_CODES.M)
public class TestConnection extends Connection {
    public TestConnection() {
        super();
        //setConnectionProperties(PROPERTY_SELF_MANAGED);
        setConnectionProperties(PROPERTY_SELF_MANAGED | CAPABILITY_HOLD | CAPABILITY_SUPPORT_HOLD);
        setAudioModeIsVoip(true);
        setInitializing();
    }

    //todo add files into git

    @Override
    public void onCallAudioStateChanged(CallAudioState state) {
        Log.d("TestConnectionService", "onCallAudioStateChanged to " + state);
    }

    @Override
    public void onDisconnect() {
        Log.d("TestConnectionService", "onDisconnect");
        TestConnectionServiceApp.getInstance().getConnectionServiceHelper().endCall(DisconnectCause.LOCAL);
    }

    @Override
    public void onAnswer() {
        Log.d("TestConnectionService", "onAnswer");
        TestConnectionServiceApp.getInstance().getConnectionServiceHelper().acceptCall2();
    }

    @Override
    public void onShowIncomingCallUi() {
        Log.d("TestConnectionService", "onShowIncomingCallUi()");
        TestConnectionServiceApp.getInstance().getConnectionServiceHelper().notifyRinging();
        // startRinging();
    }

    @Override
    public void onStateChanged(int state) {
        super.onStateChanged(state);
        Log.d("TestConnectionService", "onStateChanged to " + state);
    }

    @Override
    public void onCallEvent(String event, Bundle extras) {
        super.onCallEvent(event, extras);
        Log.d("TestConnectionService", "onCallEvent to " + event);
    }

    // undocumented API
    //@Override
    public void onSilence() {
        Log.d("TestConnectionService", "onSilence");
    }

    @Override
    public void onReject() {
        Log.d("TestConnectionService", "onReject");
        TestConnectionServiceApp.getInstance().getConnectionServiceHelper().endCall(DisconnectCause.REJECTED);
    }

    @Override
    public void onHold() {
        super.onHold();
        Log.d("TestConnectionService", "onHold");
    }

    @Override
    public void onUnhold() {
        super.onUnhold();
        Log.d("TestConnectionService", "onUnhold");
    }
}
